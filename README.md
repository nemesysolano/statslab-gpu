# Minimalistic Numeric Library For node.js #

A node.js native module that provides bindings to a subset of Intel MKL functions which deal with dense matrices.

## Linux Requirements ##

### Libraries ###

1. Intel MKL (mkl_intel_thread, mkl_rt, mkl_core, mkl_intel_lp64)
2. POSIX threads (pthread).
3. Intel OpenMP (iomp5)
					
### Tool Chain ###

1. node-gyp
2. GNU c++ 5.0 (g++)
3. node.js 8+

## Windows Requirements ##

I will port to Windows right after I finish linux implementation. Please, be patient.

## Definitions ##
1. LU Factorization: https://en.wikipedia.org/wiki/LU_decomposition
2. Cholesky Factorization: https://en.wikipedia.org/wiki/Cholesky_decomposition
 * Positive-definite matrix: https://en.wikipedia.org/wiki/Positive-definite_matrix
 * Hermitian Matrix: https://en.wikipedia.org/wiki/Positive-definite_matrix
 * Conjugate transpose: https://en.wikipedia.org/wiki/Conjugate_transpose
