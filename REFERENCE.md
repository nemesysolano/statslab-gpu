# Data Objects #

## minum.model.Result ##

Most functions actually return instances of **minum.Result** containing operation result alongside a status code. A non-zero status
code means that something went wrong and consequently the result can't be trusted.

### minum.model.Result.output ###

Returns

- *minum.Matrix*. The resulting matrix.

### minum.model.Result.status ###

Returns

- *int*. Status code.


## minum.model.Matrix ##

This class represents an **m**(rows)×**n**(columns) real matrix.

### minum.model.Matrix(rows, columns) ###

This is the class constructor

Parameters

1. *int rows*. Vertical dimension
2. *int colums*. Horizontal dimension

Returns

*minum.Matrix*. A new matrix with entries initialized to 0.

### minum.model.Matrix.rows() ###

Returns

- *int*. Vertical dimension

### minum.model.Matrix.columns() ###

Returns

- *int*. Horizontal dimension

### minum.model.Matrix.at(m, n, value) ###

This function sets or retrieves entries.

Parameters

1. *int m*. Cell row.
2. *int n*. Cell column
3. *optional double value*. Value to store at (m,n) cell. 

Returns

- *double*. Value at (m,n) cell. If *value* parameter is provided, then (m,n) cell is updated before returning.


# Functions #

## Descriptive Statistics ##

### minum.computational.statistics.rowSum(matrix) ###

Computes the sum of each row.

Parameters

1. *minum.model.Matrix matrix*. 

Returns

- *minum.Matrix*. A **column** vector containing the sum of each row.

### minum.computational.statistics.columnSum(matrix) ###

Computes the sum of each column.

Parameters

1. *minum.model.Matrix matrix*. 

Returns

- *minum.model.Matrix*. A **row** vector containing the sum of each column.

### minum.computational.statistics.rowSkewness(matrix) ###

Computes the skewness of each row.

Parameters

1. *minum.model.Matrix matrix*. 

Returns

- *minum.model.Matrix*. A **column** vector containing the skewness of each row.

### minum.computational.statistics.columnSkewness(matrix) ###

Computes the skewness of each column.

Parameters

1. *minum.model.Matrix matrix*.

Returns

- *minum.model.Matrix*.  A **row** vector containing the skewness of each column.

### minum.computational.statistics.rowKurtosis(matrix) ###

Computes the kurtosis of each row.

Parameters

1. *minum.model.Matrix matrix*. 

Returns

- *minum.Matrix*. A **column** vector containing the kurtosis of each row.

### minum.computational.statistics.columnKurtosis(matrix) ###

Computes the kurtosis of each column.

Parameters

1. *minum.model.Matrix matrix*.

Returns

- *minum.model.Matrix*.  A **row** vector containing the kurtosis of each column.

### minum.computational.statistics.rowVar(matrix) ###

Computes the variance of each row.

Parameters

1. *minum.model.Matrix matrix*. 

Returns

- *minum.model.Matrix*.  **column** vector containing the variance of each row.

### minum.computational.statistics.columnVar(matrix) ###

Computes the variance of each column.

Parameters

1. *minum.model.Matrix matrix*. 

Returns

- *minum.model.Matrix*.  A **row** vector containing the variance of each column.

- *minum.model.Matrix*.  A **row** vector containing the kurtosis of each column.

### minum.computational.statistics.rowAvg(matrix) ###

Computes the average of each row.

Parameters

1. *minum.model.Matrix matrix*. 

Returns

- *minum.model.Matrix*.  **column** vector containing the average of each row.

### minum.computational.statistics.columnAvg(matrix) ###

Computes the average of each column.

Parameters

1. *minum.model.Matrix matrix*. 

Returns

- *minum.model.Matrix.Matrix*.  **row** vector containing the average of each column.

## Linear Algebra ##

The **output** parameter in non-orthogonal factorization functions --namely LU & Cholesky--  is overwritten and returned as **minum.Result.output** property value. Otherwise, when **output** is not provided, a new matrix containing the expected result is returned.

In orthogonal factorization functions --namely QR && LQ--, both **input** and **output** parameter are overwritten. 

Linear solvers overwrite **B** parameter with solution and return it as **minum.Result.output** when **destroy** == **true**. When **destroy** == **false** or not provided, then a new matrix containing solution is returned.

### LUFactorization(input, output)  ###

Computes the LU factorization of a general m-by-n matrix.

Parameters

1. *minum.Matrix input*. Square matrix.
2. *optional minum.Matrix output*. Output matrix. A max(m,n)-by-max(m,n) matrix.

Returns

- *minum.Result*. The **output** property is a matrix containing **L** and **U** factors; the unit diagonal elements of L are not stored.

### LUSolve(A, B, destroy)  ###

Finds the solution matrix *X* for *AX = B* using LU factorization.

Parameters

1. *minum.Matrix A*. Square matrix.
2. *minum.Matrix B*. A matrix containg whose row count matches A's.
3. *optional boolean destroy*.

Returns

- *minum.Result*. The **output** property is a matrix containing system solutions.

### UCholeskyFactorization(input, output) ###

Computes the Cholesky factorization of a symmetric (Hermitian) positive-definite matrix.

Parameters

1. *minum.Matrix input*. Symmetric (Hermitian) positive-definite matrix.
2. *optional minum.Matrix output*. Output matrix whose dimension match input's.

Returns

- *minum.Result*. The **output** property is a matrix containing the Cholesky **U** factor in the upper triangular part.

### UCholeskySolve(A, B, destroy)  ###

Finds the solution matrix *X* for *AX = B* using Upper Cholesky Factorization (*UCholeskyFactorization*).

Parameters

1. *minum.Matrix A*. Square matrix.
2. *minum.Matrix B*. A matrix containg whose row count matches A's.
3. *optional boolean destroy*.

Returns

- *minum.Result*. The **output** property is a matrix containing system solutions.

### LCholeskyFactorization(input, output) ###

Computes the Cholesky factorization of a symmetric (Hermitian) positive-definite matrix.

Parameters

1. *minum.Matrix input*. Symmetric (Hermitian) positive-definite matrix.
2. *optional minum.Matrix output*. Output matrix whose dimension match input's.

Returns

- *minum.Result*. The **output** property is a matrix containing the Cholesky **L** factor in the lower triangular part.

### LCholeskySolve(A, B, destroy)  ##

Finds the solution matrix *X* for *AX = B* using Upper Lower Factorization (*LCholeskyFactorization*).

Parameters

1. *minum.Matrix A*. Square matrix.
2. *minum.Matrix B*. A matrix containg whose row count matches A's.
3. *optional boolean destroy*.

Returns

- *minum.Result*. The **output** property is a matrix containing system solutions.

### QRFactorization(input, output) ###

Computes the QR factorization of a general m-by-n matrix.

Parameters

1. *minum.Matrix input*. The function expects that **input.rows()** ≥ **output.columns()**.
2. *optional minum.Matrix output*. A symmetric matrix such that **input.rows()** == **output.rows()**.

Returns

- *minum.Result*. The **output** property is a matrix containing **Q** factor. The **R** factor can be computed by **minum.product(minum.transpose(Q), A)**, where **A** is cloned from **input** before invoking this function.

### LQFactorization(input, output) ###

Computes the QR factorization of a general m-by-n matrix.

Parameters

1. *minum.Matrix input*. The function expects that **input.rows()** ≥ **output.columns()**.
2. *optional minum.Matrix output*. A symmetric matrix such that **input.rows()** == **output.rows()**.

Returns

- *minum.Result*. The **output** property is a matrix containing **Q** factor. The **L** factor can be computed by **minum.product(A, minum.transpose(Q))**, where **A** is cloned from **input** before invoking this function.

### RQFactorization(input, output) ###

Computes the RQ factorization of a general m-by-n matrix.

Parameters

1. *minum.Matrix input*. The function expects that **input.rows()** ≥ **output.columns()**.
2. *optional minum.Matrix output*. A symmetric matrix such that **input.rows()** == **output.rows()**.

Returns

- *minum.Result*. The **output** property is a matrix containing **Q** factor. The **R** factor can be computed by **minum.product(A, minum.transpose(Q))**, where **A** is cloned from **input** before invoking this function.

### QLFactorization(input, output) ###

Computes the QL factorization of a general m-by-n matrix.

Parameters

1. *minum.Matrix input*. The function expects that **input.rows()** ≥ **output.columns()**.
2. *optional minum.Matrix output*. A symmetric matrix such that **input.rows()** == **output.rows()**.

Returns

- *minum.Result*. The **output** property is a matrix containing **Q** factor. The **L** factor can be computed by **minum.product(minum.transpose(Q), A)**, where **A** is cloned from **input** before invoking this function.
