#include "factorization.h"
#include <mkl.h>
#include <string.h>
#include "utilities.h"
//
// https://medium.com/netscape/tutorial-building-native-c-modules-for-node-js-using-nan-part-1-755b07389c7c
// https://stackoverflow.com/questions/35834294/implementing-qr-decomposition-in-c
//


using namespace utilities;

static const char* FACTOR_ERROR_MESSAGES[] = {
	"@factorization::UCholeskyFactorization (matrix::Matrix & inout). Argument is not a square matrix",
	"@factorization::LCholeskyFactorization (matrix::Matrix & inout). Argument is not a square matrix",
	"@factorization::QRFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T): A.Rows() < A.Columns()",
	"@factorization::LQFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T): A.Rows() < A.Columns()",
	"@factorization::RQFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T): A.Rows() < A.Columns()",
	"@factorization::QLFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T): A.Rows() < A.Columns()",
};

#define UCHOLESKY_ARG_NOT_SQUARE 0
#define LCHOLESKY_ARG_NOT_SQUARE (UCHOLESKY_ARG_NOT_SQUARE + 1)
#define QRFACTORIZATION_FEW_ROWS (LCHOLESKY_ARG_NOT_SQUARE + 1)
#define LQFACTORIZATION_FEW_ROWS (QRFACTORIZATION_FEW_ROWS + 1)
#define RQFACTORIZATION_FEW_ROWS (LQFACTORIZATION_FEW_ROWS + 1)
#define QLFACTORIZATION_FEW_ROWS (RQFACTORIZATION_FEW_ROWS + 1)

int factorization::LUFactorization (matrix::Matrix & inout, lapack_int * ipiv){
	size_t m = inout.Rows();
	size_t n = inout.Columns();
	size_t lda = max(1, n);
	lapack_int  result = LAPACKE_dgetrf (LAPACK_ROW_MAJOR , m , n , inout.Buffer() , lda , ipiv);
	return result;
}


int factorization::LUFactorization (matrix::Matrix & inout) {
	lapack_int * ipiv = (lapack_int *)malloc(min(inout.Rows(), inout.Columns()) * sizeof(lapack_int));
	int result = LUFactorization(inout, ipiv);
	free(ipiv);
	return result;
}

int factorization::LUFactorization (matrix::Matrix & in, matrix::Matrix &out) {
	utilities::Copy(in, out);
	return LUFactorization(out);
}


int factorization::UCholeskyFactorization (matrix::Matrix & inout) {
	if(inout.Rows() != inout.Columns())
		THROW_ERROR(UCHOLESKY_ARG_NOT_SQUARE, FACTOR_ERROR_MESSAGES);

	int result = LAPACKE_dpotrf (
		LAPACK_ROW_MAJOR,
		'L',
		inout.Rows(),
		inout.Buffer(),
		inout.Rows()
	);
	return result;
}

int factorization::UCholeskyFactorization (matrix::Matrix & in, matrix::Matrix &out){
	utilities::Copy(in, out);
	return UCholeskyFactorization(out);
}

int factorization::LCholeskyFactorization (matrix::Matrix & inout) {
	if(inout.Rows() != inout.Columns())
		THROW_ERROR(LCHOLESKY_ARG_NOT_SQUARE, FACTOR_ERROR_MESSAGES);

	int result =  LAPACKE_dpotrf (
		LAPACK_ROW_MAJOR,
		'U',
		inout.Rows(),
		inout.Buffer(),
		inout.Rows()
	);

	return result;
}

int factorization::LCholeskyFactorization (matrix::Matrix & in, matrix::Matrix &out){
	utilities::Copy(in, out);
	return LCholeskyFactorization(out);
}


int factorization::QRFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T) {
	if(A.Rows() < A.Columns())
		THROW_ERROR(QRFACTORIZATION_FEW_ROWS, FACTOR_ERROR_MESSAGES);

	size_t m = A.Rows();
	size_t n = A.Columns();
	size_t lda = max(1,n);
	lapack_int result = LAPACKE_dgeqrf (
		LAPACK_ROW_MAJOR,
		m,
		n,
		A.Buffer(),
		lda,
		T.Buffer()
	);

	return result;
}

int factorization::QRFactorization (/* inout */ matrix::Matrix & A, /* inout Q*/ matrix::Matrix & out) {
	const size_t m = A.Rows();
	const size_t n = A.Columns();
	const size_t rank = min(m, n);
	double * _Q = out.Buffer();
	double * _A = A.Buffer();
	matrix::Matrix T(1, max (1, min(m, n)));
	int result = factorization::QRFactorizationImpl(A, T);
	size_t row;

	if(result == 0){
	    // Create orthogonal matrix Q (in tmpA)
	     LAPACKE_dorgqr(LAPACK_ROW_MAJOR, m, rank, rank, _A, n, T.Buffer());


	     //Copy Q (_m x rank) into position
	     if(m == n) {
	         memcpy(_Q, _A, sizeof(double)*(m*n));
	     } else {

#pragma omp parallel for
	         for(row =0; row < m; ++row) {
	             memcpy(_Q+row*rank, _A+row*n, sizeof(double)*(rank));
	         }
	     }
	}

	return result;
}

int factorization::RQFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T) {
	if(A.Rows() < A.Columns())
		THROW_ERROR(RQFACTORIZATION_FEW_ROWS, FACTOR_ERROR_MESSAGES);

	size_t m = A.Rows();
	size_t n = A.Columns();
	size_t lda = max(1,n);
	lapack_int result = LAPACKE_dgerqf (
		LAPACK_ROW_MAJOR,
		m,
		n,
		A.Buffer(),
		lda,
		T.Buffer()
	);

	return result;
}

int factorization::RQFactorization (/* inout */ matrix::Matrix & A, /* inout Q */ matrix::Matrix & out) {
	const size_t m = A.Rows();
	const size_t n = A.Columns();
	const size_t rank = min(m, n);
	double * _Q = out.Buffer();
	double * _A = A.Buffer();
	matrix::Matrix T(1, max (1, min(m, n)));
	int result = factorization::RQFactorizationImpl(A, T);
	size_t row;

	if(result == 0){
	    // Create orthogonal matrix Q (in tmpA)
	     LAPACKE_dorgrq(LAPACK_ROW_MAJOR, m, rank, rank, _A, n, T.Buffer());


	     //Copy Q (_m x rank) into position
	     if(m == n) {
	         memcpy(_Q, _A, sizeof(double)*(m*n));
	     } else {

#pragma omp parallel for
	         for(row =0; row < m; ++row) {
	             memcpy(_Q+row*rank, _A+row*n, sizeof(double)*(rank));
	         }
	     }
	}

	return result;
}

int factorization::LQFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T) {
	if(A.Rows() < A.Columns())
		THROW_ERROR(LQFACTORIZATION_FEW_ROWS, FACTOR_ERROR_MESSAGES);

	size_t m = A.Rows();
	size_t n = A.Columns();
	size_t lda = max(1,n);
	lapack_int result = LAPACKE_dgelqf (
		LAPACK_ROW_MAJOR,
		m,
		n,
		A.Buffer(),
		lda,
		T.Buffer()
	);

	return result;
}

int factorization::LQFactorization (/* inout */ matrix::Matrix & A, /* inout Q */ matrix::Matrix & out) {
	const size_t m = A.Rows();
	const size_t n = A.Columns();
	const size_t rank = min(m, n);
	double * _Q = out.Buffer();
	double * _A = A.Buffer();
	matrix::Matrix T(1, max (1, min(m, n)));
	int result = factorization::LQFactorizationImpl(A, T);
	size_t row;

	if(result == 0){
	    // Create orthogonal matrix Q (in tmpA)
	     LAPACKE_dorglq(LAPACK_ROW_MAJOR, m, rank, rank, _A, n, T.Buffer());


	     //Copy Q (_m x rank) into position
	     if(m == n) {
	         memcpy(_Q, _A, sizeof(double)*(m*n));
	     } else {

#pragma omp parallel for
	         for(row =0; row < m; ++row) {
	             memcpy(_Q+row*rank, _A+row*n, sizeof(double)*(rank));
	         }
	     }
	}

	return result;
}

int factorization::QLFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T) {
	if(A.Rows() < A.Columns())
		THROW_ERROR(QLFACTORIZATION_FEW_ROWS, FACTOR_ERROR_MESSAGES);

	size_t m = A.Rows();
	size_t n = A.Columns();
	size_t lda = max(1,n);
	lapack_int result = LAPACKE_dgeqlf (
		LAPACK_ROW_MAJOR,
		m,
		n,
		A.Buffer(),
		lda,
		T.Buffer()
	);

	return result;
}

int factorization::QLFactorization (/* inout */ matrix::Matrix & A, /* inout Q */ matrix::Matrix & out) {
	const size_t m = A.Rows();
	const size_t n = A.Columns();
	const size_t rank = min(m, n);
	double * _Q = out.Buffer();
	double * _A = A.Buffer();
	matrix::Matrix T(1, max (1, min(m, n)));
	int result = factorization::QLFactorizationImpl(A, T);
	size_t row;

	if(result == 0){
	    // Create orthogonal matrix Q (in tmpA)
	     LAPACKE_dorgql(LAPACK_ROW_MAJOR, m, rank, rank, _A, n, T.Buffer());


	     //Copy Q (_m x rank) into position
	     if(m == n) {
	         memcpy(_Q, _A, sizeof(double)*(m*n));
	     } else {

#pragma omp parallel for
	         for(row =0; row < m; ++row) {
	             memcpy(_Q+row*rank, _A+row*n, sizeof(double)*(rank));
	         }
	     }
	}

	return result;
}
