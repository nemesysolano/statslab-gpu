/*
 * memory.cpp
 *
 *  Created on: 12-Aug-2018
 *      Author: rsolano
 */


#include <matrix.h>
#include "utilities.h"
#include <string.h>

static const char* UTIL_ERROR_MESSAGES[] = {
	"@utilities::Copy(matrix::Matrix & in, matrix::Matrix &out): Dimension mismatch.",
	"@utilities::Lower(matrix::Matrix & in, matrix::Matrix &out). Dimension  mismatch",
	"@utilities::Upper(matrix::Matrix & in, matrix::Matrix &out). Dimension mismtch",
};

#define COPY_DIM_MISMATCH 0
#define LOWER_DIM_MISMATCH (COPY_DIM_MISMATCH + 1)
#define UPPER_DIM_MISMATCH (LOWER_DIM_MISMATCH + 1)
using namespace utilities;

void utilities::Copy(matrix::Matrix & in, matrix::Matrix &out, size_t error_code) {
	if(!(in.Rows() == out.Rows() && in.Columns() == out.Columns()))
		THROW_ERROR(error_code, UTIL_ERROR_MESSAGES);

	double * buffer_in = in.Buffer();
	double * buffer_out = out.Buffer();
	size_t total_size = sizeof(double) * in.Rows() * in.Columns();
	memcpy(buffer_out, buffer_in, total_size);
}

void utilities::Copy(matrix::Matrix & in, matrix::Matrix &out) {
	Copy(in, out, COPY_DIM_MISMATCH);
}


void utilities::Lower(matrix::Matrix & in, matrix::Matrix &out) {
	if(!(in.Rows() == out.Rows() && in.Columns() == out.Columns()))
		THROW_ERROR(LOWER_DIM_MISMATCH, UTIL_ERROR_MESSAGES);

	size_t i;
	const size_t rows = in.Rows();

#pragma omp parallel for
	for(i = 0; i < rows; i++) {
		size_t k;

		for(k = 0 ; k < i; k++) {
			out(k,i) = 0.0;
		}

		while(k < rows) {
			out(k,i) = in(k, i);
			k++;
		}
	}
}

void utilities::Upper(matrix::Matrix & in, matrix::Matrix &out) {
	if(!(in.Rows() == out.Rows() && in.Columns() == out.Columns()))
		THROW_ERROR(UPPER_DIM_MISMATCH, UTIL_ERROR_MESSAGES);

	int i;
	const int rows = in.Rows();
	const int bottom = rows - 1;

#pragma omp parallel for
	for(i = 0; i < rows; i++) {
		int k;

		for(k = bottom ; k > i; k--) {
			out(k,i) = 0.0;
		}

		while(k > -1) {
			out(k,i) = in(k, i);

			k--;
		}
	}
}
