################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../condition-number.cpp \
../factorization.cpp \
../linear-eq.cpp \
../refining-solution.cpp \
../utilities.cpp 

OBJS += \
./condition-number.o \
./factorization.o \
./linear-eq.o \
./refining-solution.o \
./utilities.o 

CPP_DEPS += \
./condition-number.d \
./factorization.d \
./linear-eq.d \
./refining-solution.d \
./utilities.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I"/home/rsolano/workspaces/statslab-gpu/matrix" -I/opt/intel/mkl/include/ -O0 -g3 -Wall -c -fmessage-length=0 -fopenmp -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


