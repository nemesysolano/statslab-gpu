/*
 * linear-eq.h
 *
 *  Created on: 09-Aug-2018
 *      Author: rsolano
 */

#ifndef LINEAR_EQ_H_
#define LINEAR_EQ_H_
#include "factorization.h"

namespace solver {
	int LUSolve(/* inout (LU factored) */ matrix::Matrix & A, /* inout (right-hand matrix) */ matrix::Matrix & B);
	int LUSolve(matrix::Matrix & A, /* out */ matrix::Matrix & LU, /* inout */ matrix::Matrix & B);

	int LCholeskySolve(/* inout (L factored) */ matrix::Matrix & A, /* inout (right-hand matrix) */ matrix::Matrix & B);
	int LCholeskySolve(matrix::Matrix & A, matrix::Matrix & L, /* inout (right-hand matrix) */ matrix::Matrix & B);

	int UCholeskySolve(/* inout (U factored) */ matrix::Matrix & A, /* inout (right-hand matrix) */ matrix::Matrix & B);
	int UCholeskySolve(matrix::Matrix & A, matrix::Matrix & U, /* inout (right-hand matrix) */ matrix::Matrix & B);

}

#endif /* LINEAR_EQ_H_ */
