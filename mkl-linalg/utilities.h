/*
 * utilities.h
 *
 *  Created on: 12-Aug-2018
 *      Author: rsolano
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_


#define max(a,b) (a > b ? a: b)
#define min(a,b) (a < b ? a: b)

namespace utilities {
	void Copy(matrix::Matrix & in, matrix::Matrix &out);
	void Copy(matrix::Matrix & in, matrix::Matrix &out, size_t error_code);
	void Lower(matrix::Matrix & in, matrix::Matrix &out);
	void Upper(matrix::Matrix & in, matrix::Matrix &out);
}


#endif /* UTILITIES_H_ */
