/*
 * factorization.h
 *
 *  Created on: 09-Aug-2018
 *      Author: rsolano
 */

#ifndef FACTORIZATION_H_
#define FACTORIZATION_H_
#include <matrix.h>
#include <mkl.h>

namespace factorization {
	int LUFactorization (matrix::Matrix & inout, lapack_int * ipiv);
	int LUFactorization (matrix::Matrix & inout);
	int LUFactorization (matrix::Matrix & in, matrix::Matrix &out);

	int UCholeskyFactorization (matrix::Matrix & inout) ;
	int UCholeskyFactorization (matrix::Matrix & in, matrix::Matrix &out);
 
	int LCholeskyFactorization (matrix::Matrix & inout) ;
	int LCholeskyFactorization (matrix::Matrix & in, matrix::Matrix &out);

	int QRFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T);
	int QRFactorization (/* inout */ matrix::Matrix & A, /* inout Q */ matrix::Matrix & out);

	int RQFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T);
	int RQFactorization (/* inout */ matrix::Matrix & A, /* inout Q */ matrix::Matrix & out);

	int LQFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T);
	int LQFactorization (/* inout */ matrix::Matrix & A, /* inout Q */ matrix::Matrix & out);

	int QLFactorizationImpl (/* inout */ matrix::Matrix & A, /* inout */ matrix::Matrix & T);
	int QLFactorization (/* inout */ matrix::Matrix & A, /* inout Q */ matrix::Matrix & out);
}

#endif /* FACTORIZATION_H_ */
