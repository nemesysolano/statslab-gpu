/*
 * linear-eq.cpp
 *
 *  Created on: 09-Aug-2018
 *      Author: rsolano
 */



#include "factorization.h"
#include <mkl.h>
#include <string.h>
#include "linear-eq.h"
#include "utilities.h"
#include <matrix.h>

#define LUSOLVE_DIM_MISMATCH 0
using namespace solver;
using namespace factorization;
using namespace utilities;

static const char* SOLVER_ERROR_MESSAGES[] = {
	"@solver::LUSolve(/* inout */ matrix::Matrix & A, /* inout */  matrix::Matrix & B",
	"@solver::LCholeskySolve(/* inout (L factored) */ matrix::Matrix & A, /* inout (right-hand matrix) */ matrix::Matrix & B).",
	"@solver::UCholeskySolve(/* inout (L factored) */ matrix::Matrix & A, /* inout (right-hand matrix) */ matrix::Matrix & B)."
};

#define LUSOLVE_DIM_MISMATCH 0
#define LCHOLESKY_DIM_MISMATCH (LUSOLVE_DIM_MISMATCH + 1)
#define UCHOLESKY_DIM_MISMATCH (UCHOLESKY_DIM_MISMATCH + 1)

int solver::LUSolve(/* inout */ matrix::Matrix & A, /* inout */  matrix::Matrix & B) {

	if(A.Rows() != B.Rows() || A.Rows() == 1)
		THROW_ERROR(LUSOLVE_DIM_MISMATCH, SOLVER_ERROR_MESSAGES);

	size_t ipiv_size = max(1, min(A.Rows(), A.Columns())) ;
	lapack_int * ipiv = (lapack_int *)malloc(ipiv_size * sizeof(lapack_int));
	lapack_int  result = LUFactorization(A, ipiv);

	if(result == 0) {
		size_t n = B.Rows();
		size_t m = B.Columns();
		size_t lda = max(1, n);
		size_t ldb = m;

		result = LAPACKE_dgetrs(LAPACK_ROW_MAJOR, 'T', n, m, A.Buffer(), lda, ipiv, B.Buffer(), ldb );

	}

	free(ipiv);
	return result;
}

int solver::LUSolve(matrix::Matrix & A, /* inout */ matrix::Matrix & LU, /* inout */ matrix::Matrix & B) {
	utilities::Copy(A, LU);
	return LUSolve(LU, B);
}

int solver::LCholeskySolve(/* inout (L factored) */ matrix::Matrix & A, /* inout (right-hand matrix) */ matrix::Matrix & B) {
	if(A.Columns() < A.Rows() || A.Rows() != B.Rows())
		THROW_ERROR(LCHOLESKY_DIM_MISMATCH, SOLVER_ERROR_MESSAGES);

	lapack_int  result = LCholeskyFactorization(A);

	if(result == 0) {
		size_t n = A.Rows();
		size_t m = B.Columns(); //nrhs
		size_t lda = max(1, n);
		size_t ldb = m;

		return  LAPACKE_dpotrs (
			LAPACK_ROW_MAJOR,
			'U',
			n,
			m,
			A.Buffer(),
			lda,
			B.Buffer(),
			ldb
		);
	}

	return result;
}

int solver::LCholeskySolve(matrix::Matrix & A, matrix::Matrix & L, /* inout (right-hand matrix) */ matrix::Matrix & B) {
	utilities::Copy(A, L);
	return LCholeskySolve(L, B);
}

int solver::UCholeskySolve(/* inout (U factored) */ matrix::Matrix & A, /* inout (right-hand matrix) */ matrix::Matrix & B) {
	if(A.Columns() < A.Rows() || A.Rows() != B.Rows())
		THROW_ERROR(LCHOLESKY_DIM_MISMATCH, SOLVER_ERROR_MESSAGES);

	lapack_int  result = UCholeskyFactorization(A);

	if(result == 0) {
		size_t n = A.Rows();
		size_t m = B.Columns(); //nrhs
		size_t lda = max(1, n);
		size_t ldb = m;

		return  LAPACKE_dpotrs (
			LAPACK_ROW_MAJOR,
			'L',
			n,
			m,
			A.Buffer(),
			lda,
			B.Buffer(),
			ldb
		);
	}

	return result;
}

int solver::UCholeskySolve(matrix::Matrix & A, matrix::Matrix & U, /* inout (right-hand matrix) */ matrix::Matrix & B) {
	utilities::Copy(A, U);
	return UCholeskySolve(U, B);
}
