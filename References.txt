

1. Computes the Cholesky factorization of a symmetric (Hermitian) positive-definite matrix. (https://software.intel.com/en-us/mkl-developer-reference-c-potrf)
2. Solves a system of linear equations with a Cholesky-factored symmetric (Hermitian) positive-definite coefficient matrix.. (https://software.intel.com/en-us/mkl-developer-reference-c-potrs)