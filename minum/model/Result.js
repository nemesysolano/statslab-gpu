class Result {
    constructor (output, status) {
        this._output = output;
        this._status = status;
    }

    get status() { return this._status; }
    get output() { return this._output;}
}

module.exports = Result;