const impl = require('../native/build/Release/minumimpl');
const Matrix = require('../model/Matrix') ;
const Result = require('../model/Result');

const unaryOperation = (input, output, fn) => {if(!isMatrix(output)) return null; fn(output); return output;};
const binaryOperation = (a, b, out, fn, check) => null ? !check(a,b) : fn(a, b, out);
const isMatrix = (matrix) => matrix && matrix instanceof Matrix;
const isSquareMatrix = (matrix) => isMatrix(matrix) && matrix.rows() == matrix.columns();
const compatibleForSum = (a, b) => isMatrix(a) && isMatrix(b) && a.rows() == b.rows() && a.columns() == b.columns();
const compatibleForProduct = (a, b) => isMatrix(a) && isMatrix(b) && a.columns() == b.rows();
const transform = (matrix, fn) => null ? !isMatrix(matrix) : fn(matrix);
const sum =  (a, b) => binaryOperation(
    a, 
    b, 
    Matrix(a.rows(), a.columns()), 
    (a,b,out) => {a.add(b,out); return out;} , 
    compatibleForSum
);

const sub =  (a, b) => binaryOperation(
    a, 
    b, 
    Matrix(a.rows(), a.columns()), 
    (a,b,out) => {a.sub(b,out); return out;} , 
    compatibleForSum
);

const matrixProduct = (a, b) => binaryOperation(
    a, 
    b, 
    Matrix(a.rows(), b.columns()), 
    (a,b,out) => {a.product(b,out); return out;} , 
    compatibleForProduct
);

const Lower2 = (input, output) => compatibleForSum(input, output) ? ((result) => new Result(output, result)) (impl.Lower(input, output)) : null;
const Lower = (input, output) =>  (typeof output == "undefined") ? Lower2(input, Matrix(input.rows(), input.columns())) : Lower2(input, output);

const Upper2 = (input, output) => compatibleForSum(input, output) ? ((result) => new Result(output, result)) (impl.Upper(input, output)) : null;
const Upper = (input, output) =>  (typeof output == "undefined") ? Upper2(input, Matrix(input.rows(), input.columns())) : Upper2(input, output);  

exports.sum = sum;
exports.sub = sub;
exports.isMatrix = isMatrix;
exports.isSquareMatrix = isSquareMatrix;
exports.compatibleForSum = compatibleForSum;
exports.compatibleForProduct = compatibleForProduct;
exports.transform = transform;
exports.transpose = (matrix) => unaryOperation(matrix, Matrix(matrix.columns(), matrix.rows()), (out)=> matrix.transpose(out));
exports.clone = (matrix) => {const out = Matrix(matrix.rows(), matrix.columns()) ;matrix.clone(out); return out;};
exports.unaryOperation = unaryOperation;
exports.binaryOperation = binaryOperation;
exports.scalarProduct = (v, a) => 
    !(typeof v == "number" && a instanceof Matrix) ? null : 
    unaryOperation(a, Matrix(a.rows(), a.columns()), (output)=> a.scalarProduct(v, output));

exports.matrixProduct = matrixProduct;
exports.product =  (x, y) => compatibleForProduct(x, y) ? matrixProduct(x, y) : !(typeof x == "number") ? null: scalarProduct(x, y);
exports.Lower = Lower;
exports.Upper = Upper;
exports.rowVector = (matrix)  => transform(matrix, (matrix)=> Matrix(1, matrix.columns()));
exports.columnVector = (matrix)  => transform(matrix, (matrix)=> Matrix(matrix.rows(),1)); 

