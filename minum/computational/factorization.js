'use strict';
const impl = require('../native/build/Release/minumimpl');
const Matrix = require('../model/Matrix') ;
const Result = require('../model/Result');
const transformation = require('./transformation');
const compatibleForSum = transformation.compatibleForSum;
const clone = transformation.clone;
const isMatrix = transformation.isMatrix;

//Linear Algebra API
const LUFactorization2 = (input, output) => compatibleForSum(input, output) ? ((result) => new Result(output, result)) (impl.LUFactorization(input, output)) : null;
const LUFactorization = (input, output) => (typeof output == "undefined") ? LUFactorization2(input, Matrix(input.rows(), input.columns())) : LUFactorization2(input, output);

const LUSolve2 = (input, output) =>  isMatrix(input) && isMatrix(output) && input.rows() == output.rows() ? ((result) => new Result(output, result)) (impl.LUSolve(input, output)) : null;
const LUSolve = (A,B, destroy) => ((typeof destroy == "boolean") && destroy) ? LUSolve2(A,B) : LUSolve2(clone(A), clone(B));

const UCholeskyFactorization2 = (input, output) => compatibleForSum(input, output) ? ((result) => new Result(output, result)) (impl.UCholeskyFactorization(input, output)) : null;
const UCholeskyFactorization = (input, output) => (typeof output == "undefined") ? UCholeskyFactorization2(input, Matrix(input.rows(), input.columns())) : UCholeskyFactorization2(input, output);
const UCholeskySolve2 = (input, output) =>  isMatrix(input) && isMatrix(output) && input.rows() == output.rows() ? ((result) => new Result(output, result)) (impl.UCholeskySolve(input, output)) : null;
const UCholeskySolve = (A,B, destroy) => ((typeof destroy == "boolean") && destroy) ? UCholeskySolve2(A,B) : UCholeskySolve2(clone(A), clone(B));

const LCholeskyFactorization2 = (input, output) => compatibleForSum(input, output) ? ((result) => new Result(output, result)) (impl.LCholeskyFactorization(input, output)) : null;
const LCholeskyFactorization = (input, output) => (typeof output == "undefined") ? LCholeskyFactorization2(input, Matrix(input.rows(), input.columns())) : LCholeskyFactorization2(input, output);
const LCholeskySolve2  = (input, output) =>  isMatrix(input) && isMatrix(output) && input.rows() == output.rows() ? ((result) => new Result(output, result)) (impl.LCholeskySolve(input, output)) : null;
const LCholeskySolve = (A,B, destroy) => ((typeof destroy == "boolean") && destroy) ? LCholeskySolve2(A,B) : LCholeskySolve2(clone(A), clone(B));

const QRCompatible = (input, output) => (isMatrix(input) && isMatrix(output) && input.rows() == output.rows() && output.rows() == output.columns());
const QRFactorization2 = (input, output) =>  QRCompatible(input, output)  ? ((result) => new Result(output, result)) (impl.QRFactorization(input, output)) : null;
const QRFactorization = (input, output) => (typeof output == "undefined") ? QRFactorization2(input, Matrix(input.rows(), input.rows())) : QRFactorization2(input, output);

const LQCompatible = QRCompatible;
const LQFactorization2 = (input, output) =>  LQCompatible(input, output)  ? ((result) => new Result(output, result)) (impl.LQFactorization(input, output)) : null;
const LQFactorization = (input, output) => (typeof output == "undefined") ? LQFactorization2(input, Matrix(input.rows(), input.rows())) : LQFactorization2(input, output);

const RQCompatible = LQCompatible;
const RQFactorization2 = (input, output) =>  RQCompatible(input, output)  ? ((result) => new Result(output, result)) (impl.RQFactorization(input, output)) : null;
const RQFactorization = (input, output) => (typeof output == "undefined") ? RQFactorization2(input, Matrix(input.rows(), input.rows())) : RQFactorization2(input, output);

const QLCompatible = RQCompatible;
const QLFactorization2 = (input, output) =>  QLCompatible(input, output)  ? ((result) => new Result(output, result)) (impl.QLFactorization(input, output)) : null;
const QLFactorization = (input, output) => (typeof output == "undefined") ? QLFactorization2(input, Matrix(input.rows(), input.rows())) : QLFactorization2(input, output);


exports.LUFactorization = LUFactorization;
exports.LUSolve = LUSolve;
exports.UCholeskyFactorization  = UCholeskyFactorization;
exports.UCholeskySolve  = UCholeskySolve;
exports.LCholeskyFactorization = LCholeskyFactorization;
exports.LCholeskySolve  = LCholeskySolve;
exports.QRFactorization = QRFactorization;
exports.LQFactorization = LQFactorization;
exports.RQFactorization = RQFactorization;
exports.QLFactorization = QLFactorization
