'use strict';
const impl = require('../native/build/Release/minumimpl');
const Matrix = require('../model/Matrix') ;
const transformation = require('./transformation');

const unaryOperation = transformation.unaryOperation;
const rowSum = (matrix) => unaryOperation(matrix, transformation.columnVector(matrix), (out)=>matrix.rowSum(out));
const rowSkewness = (matrix) => unaryOperation(matrix, transformation.columnVector(matrix), (out)=>matrix.rowSkewness(out));
const rowKurtosis = (matrix) => unaryOperation(matrix, transformation.columnVector(matrix), (out)=>matrix.rowKurtosis(out));
const rowVar = (matrix) => unaryOperation(matrix, transformation.columnVector(matrix), (out)=>matrix.rowVar(out));
const rowAvg = (matrix) => unaryOperation(matrix, transformation.columnVector(matrix), (out)=>matrix.rowAvg(out));

const columnSum = (matrix) => unaryOperation(matrix, transformation.rowVector(matrix), (out)=>matrix.columnSum(out));
const columnSkewness = (matrix) => unaryOperation(matrix, transformation.rowVector(matrix), (out)=>matrix.columnSkewness(out));
const columnKurtosis = (matrix) => unaryOperation(matrix, transformation.rowVector(matrix), (out)=>matrix.columnKurtosis(out));
const columnVar = (matrix) => unaryOperation(matrix, transformation.rowVector(matrix), (out)=>matrix.columnVar(out));
const columnAvg = (matrix) => unaryOperation(matrix, transformation.rowVector(matrix), (out)=>matrix.columnAvg(out));


const binaryOperation = transformation.binaryOperation;
const sum =  (a, b) => binaryOperation(
    a, 
    b, 
    Matrix(a.rows(), a.columns()), 
    (a,b,out) => {a.add(b,out); return out;} , 
    transformation.compatibleForSum
);

const sub =  (a, b) => binaryOperation(
    a, 
    b, 
    Matrix(a.rows(), a.columns()), 
    (a,b,out) => {a.sub(b,out); return out;} , 
    transformation.compatibleForSum
);

const scalarProduct = (v, a) => 
    !(typeof v == "number" && a instanceof Matrix) ? null : 
    unaryOperation(a, Matrix(a.rows(), a.columns()), (output)=> a.scalarProduct(v, output));

const matrixProduct = (a, b) => binaryOperation(
    a, 
    b, 
    Matrix(a.rows(), b.columns()), 
    (a,b,out) => {a.product(b,out); return out;} , 
    compatibleForProduct
);
const product =  (x, y) => compatibleForProduct(x, y) ? matrixProduct(x, y) : scalarProduct(x, y);




exports.unaryOperation= unaryOperation;
exports.rowSum= rowSum;
exports.rowSkewness= rowSkewness;
exports.rowKurtosis= rowKurtosis;
exports.rowVar= rowVar;
exports.rowAvg= rowAvg;

exports.columnSum= columnSum;
exports.columnSkewness= columnSkewness;
exports.columnKurtosis= columnKurtosis;
exports.columnVar= columnVar;
exports.columnAvg= columnAvg;


exports.sum= sum;
exports.sub= sub;
exports.scalarProduct= scalarProduct;
exports.matrixProduct= matrixProduct;
exports.product= product;

