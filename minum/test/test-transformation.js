// test.js
const minum = require('../index');
const Matrix = minum.model.Matrix;
const transformation = minum.computational.transformation;
const statistics = minum.computational.statistics;

const matrix1 = Matrix(1,1);
const matrix2 = Matrix(3,2);
const matrix3 = Matrix(3,2);

matrix2.at(0,0,1)
matrix2.at(0,1,2)
matrix2.at(1,0,3)
matrix2.at(1,1,5)
matrix2.at(2,0,7)
matrix2.at(2,1,11)
matrix3.at(0,0,2)
matrix3.at(0,1,3)
matrix3.at(1,0,4)
matrix3.at(1,1,5)
matrix3.at(2,0,6)
matrix3.at(2,1,7)

matrix2.print()
console.log('---\n')

matrix3.print()
console.log('---\n')

rowSum = statistics.rowSum(matrix2) ;
columnSum = statistics.columnSum(matrix2) ;
rowSum.print()
console.log('---\n')
columnSum.print()
console.log('---\n')

transpose = transformation.transpose(matrix2);
transpose.print()
console.log('---\n')


statistics.sum(matrix2, matrix3).print();
