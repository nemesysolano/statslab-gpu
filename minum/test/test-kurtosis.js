//Credits: https://en.wikipedia.org/wiki/Kurtosis
// test.js

const minum = require('../index');
const Matrix = minum.model.Matrix;
const transformation = minum.computational.transformation;
const statistics = minum.computational.statistics;
const matrix1 = Matrix(4,2);

matrix1.at(0,0,1);matrix1.at(0,1,7);
matrix1.at(1,0,2);matrix1.at(1,1,11);
matrix1.at(2,0,3);matrix1.at(2,1,17);
matrix1.at(3,0,5);matrix1.at(3,1,19);

const matrix2 = transformation.transpose(matrix1);
const matrix3 = transformation.product(matrix1, matrix2);
const columnKurtosis = statistics.columnKurtosis(matrix3);
const rowKurtosis = statistics.rowKurtosis(matrix3);
const columnVar = statistics.columnVar(matrix3);
const rowVar = statistics.rowVar(matrix3);

matrix3.print();
console.log('\n');
console.log('Kurtosis');
columnKurtosis.print();
rowKurtosis.print();

console.log('\n');
console.log('Variance');
columnVar.print()
rowVar.print()