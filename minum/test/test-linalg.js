'use strict';

const minum = require('../index');
const Matrix = minum.model.Matrix;
const factorization = minum.computational.factorization;
const transformation = minum.computational.transformation;

function testLU() {
    const m = Matrix(3,3), M= Matrix(3,3), J = Matrix(3,5), K = Matrix(2,2), B = Matrix(3,1);
    
    m.at(0,0,5);  m.at(0,1,19); m.at(0,2,3);
    m.at(1,0,1);  m.at(1,1,7); m.at(1,2,23);
    m.at(2,0,17); m.at(2,1,2); m.at(2,2,11);

    B.at(0,0,14);
    B.at(1,0,15);
    B.at(2,0,29);
    
    K.at(0,0,0);  K.at(0,1, 0);
    K.at(1,0,1);  K.at(1,1, 0);

    let f = factorization.LUFactorization(m);
    if(f.status == 0) f.output.print(); else throw new Error('minum.LUFactorization(m) failed');
    console.log('\n')

    f = factorization.LUFactorization(m, M);
    if(f.status == 0) f.output.print(); else throw new Error('minum.LUFactorization(m, M) failed');
    console.log('\n')

    f = factorization.LUFactorization(m, J);
    if(f != null) throw new Error('minum.LUFactorization(m) failed');

    f = factorization.LUFactorization(K);
    console.log(f.status)
    if(f.status == 0) throw new Error('minum.LUFactorization(m, K) returned 0');

    f = factorization.LUSolve(m, B, false);
    f.output.print()
    transformation.product(m,f.output).print()
    console.log('\n');

    f = factorization.LUSolve(transformation.clone(m), B, true);
    f.output.print()
    transformation.product(m, B).print()    
}

function testCholesky() {
    const A = Matrix(3,3), r = Matrix(3,3), X= Matrix(3,1);
    let R, B, C, L, U;

    X.at(0,0,14);
    X.at(1,0,15);
    X.at(2,0,29);
    
    // https://en.wikipedia.org/wiki/Cholesky_decomposition

    A.at(0,0,   4); A.at(0,1, 12); A.at(0,2, -16);
    A.at(1,0,  12); A.at(1,1, 37); A.at(1,2, -43);
    A.at(2,0, -16); A.at(2,1,-43); A.at(2,2, 98);   
    B = transformation.clone(A);
    C = transformation.clone(A);
    A.print()
    console.log('\n');

   
    R = factorization.UCholeskyFactorization(A,r);
    if(R.status != 0) throw new Error("minum.UCholeskyFactorization(A,r)");
    transformation.Lower(r).output.print();
    console.log('\n');

    R = factorization.UCholeskyFactorization(A);
    if(R.status != 0) throw new Error("minum.UCholeskyFactorization(A,r)");    
    transformation.Lower(R.output).output.print();
    console.log('\n');


    console.log('\n')
    R = factorization.LCholeskyFactorization(B,r);
    if(R.status != 0) throw new Error("minum.LCholeskyFactorization(B,r)");
    transformation.Upper(r).output.print();
    console.log('\n');

    R = factorization.LCholeskyFactorization(B);
    if(R.status != 0) throw new Error("minum.LCholeskyFactorization(B)");
    transformation.Upper(R.output).output.print();
    console.log('\n')

    let Y = transformation.clone(X);
    R = factorization.LCholeskySolve(C,X);
    if(R.status != 0) throw new Error("minum.LCholeskySolve(C,X)");        
    transformation.product(C, R.output).print()
    console.log('\n')

    R = factorization.UCholeskySolve(transformation.clone(C), transformation.clone(Y),true);
    if(R.status != 0) throw new Error("minum.UCholeskySolve(C,X)");        
    transformation.product(C, R.output).print()
        
    
}

function testOrthogonal() {
    const print = (m) => {m.print(); console.log('\n');}
    const Sample = () => {
        const s = Matrix(3,3);
        s.at(0,0, 12); s.at(0,1, -51); s.at(0,2,   4);
        s.at(1,0, 6);  s.at(1,1, 167); s.at(1,2, -68);
        s.at(2,0,-4);  s.at(2,1,  24); s.at(2,2, -41);
    
        return s;
    };    

    let result = factorization.QRFactorization(Sample());
    let Q = result.output;
    let R = transformation.product(transformation.transpose(Q), Sample());
    print(Sample())
    print(transformation.product(Q, R));
    console.log('1');

    result = factorization.LQFactorization(Sample());
    Q = result.output;
    let L = transformation.product(Sample(), transformation.transpose(Q));
    print(Sample())
    print(transformation.product(L, Q));    
    console.log('2');

    result = factorization.RQFactorization(Sample());
    Q = result.output;
    R = transformation.product(Sample(), transformation.transpose(Q));
    print(Sample())
    print(transformation.product(R, Q));     
    console.log('4');

    result = factorization.QLFactorization(Sample());
    Q = result.output;
    L = transformation.product(transformation.transpose(Q), Sample());
    print(Sample())
    print(transformation.product(Q, L));         
    console.log('5');
}

testLU();
testCholesky();
testOrthogonal();