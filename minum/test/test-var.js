const minum = require('../index');
const Matrix = minum.model.Matrix;
const statistics = minum.computational.statistics;
const matrix1 = Matrix(4,2);

matrix1.at(0,0,1);matrix1.at(0,1,7);
matrix1.at(1,0,2);matrix1.at(1,1,11);
matrix1.at(2,0,3);matrix1.at(2,1,17);
matrix1.at(3,0,5);matrix1.at(3,1,19);

statistics.rowAvg(matrix1).print();
statistics.columnAvg(matrix1).print();
statistics.rowVar(matrix1).print();
statistics.columnVar(matrix1).print()