#include <node.h>
#include "matrix-wrapper.h"
#include <matrix.h>
#include <v8.h>
#include <iostream>
#include "utilities.h"

/*
Credits:
	https://github.com/v8/v8/wiki/Embedder's-Guide
*/
const char * WRONG_ARGUMENTS_ERROR="Wrong arguments. Matrix dimensions must be positive integers.";
const char * DIMS_OUTOFRANGE_ERROR="Dimensions are out of range.";
const char * INSUFFICIENT_ARGS_RROR="Please, specify matrix dimensions (m,n)";
const char * NONNUMBER_ARG_RROR="Can't store non-numeric elements in this matrix.";
const char * CLASS_NAME="Matrix";
using namespace std;

namespace numjs {
	using v8::Exception;
	using v8::Context;
	using v8::Function;
	using v8::FunctionCallbackInfo;
	using v8::FunctionTemplate;
	using v8::Isolate;
	using v8::Local;
	using v8::Object;
	using v8::Persistent;
	using v8::String;
	using v8::Value;
	using v8::Number;
	using v8::ObjectTemplate;

	Persistent<Function> MatrixWrapper::constructor;


	MatrixWrapper::MatrixWrapper(int rows, int columns) {
		this->matrix = new matrix::Matrix(0.0, rows, columns);
	}

	MatrixWrapper::MatrixWrapper(matrix::Matrix * matrix) {
		this->matrix = matrix;
	}
 
	void MatrixWrapper::Init(Local<Object> exports) {
	  Isolate* isolate = exports->GetIsolate();

	  // Prepare constructor template
	  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
	  tpl->SetClassName(String::NewFromUtf8(isolate, CLASS_NAME));
	  tpl->InstanceTemplate()->SetInternalFieldCount(1);

	  // Prototype
	  NODE_SET_PROTOTYPE_METHOD(tpl, "at", at);
	  NODE_SET_PROTOTYPE_METHOD(tpl, "rowSum", rowSum);
	  NODE_SET_PROTOTYPE_METHOD(tpl, "rowSkewness", rowSkewness);	  
	  NODE_SET_PROTOTYPE_METHOD(tpl, "rowKurtosis", rowKurtosis);	  
	  NODE_SET_PROTOTYPE_METHOD(tpl, "rowVar", rowVar);	  
	  NODE_SET_PROTOTYPE_METHOD(tpl, "rowAvg", rowAvg);	

	  NODE_SET_PROTOTYPE_METHOD(tpl, "columnSum", columnSum);
	  NODE_SET_PROTOTYPE_METHOD(tpl, "columnSkewness", columnSkewness);	  
	  NODE_SET_PROTOTYPE_METHOD(tpl, "columnKurtosis", columnKurtosis);	  	  
	  NODE_SET_PROTOTYPE_METHOD(tpl, "columnVar", columnVar);	  	  
	  NODE_SET_PROTOTYPE_METHOD(tpl, "columnAvg", columnAvg);	

	  NODE_SET_PROTOTYPE_METHOD(tpl, "rows", rows); 
	  NODE_SET_PROTOTYPE_METHOD(tpl, "columns", columns);
	  NODE_SET_PROTOTYPE_METHOD(tpl, "transpose", transpose);
	  NODE_SET_PROTOTYPE_METHOD(tpl, "add", add);
	  NODE_SET_PROTOTYPE_METHOD(tpl, "sub", sub);
	  NODE_SET_PROTOTYPE_METHOD(tpl, "product", product);
	  NODE_SET_PROTOTYPE_METHOD(tpl, "scalarProduct", scalarProduct);	  
	  NODE_SET_PROTOTYPE_METHOD(tpl, "print", print);

	  NODE_SET_PROTOTYPE_METHOD(tpl, "clone", clone);

	  constructor.Reset(isolate, tpl->GetFunction());
	  exports->Set(String::NewFromUtf8(isolate, CLASS_NAME),tpl->GetFunction());
	}

	void MatrixWrapper::rows(const v8::FunctionCallbackInfo<v8::Value>& args){
		Isolate* isolate = args.GetIsolate();
		MatrixWrapper* obj = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		args.GetReturnValue().Set(Number::New(isolate, obj->matrix->Rows()));
	}

	void MatrixWrapper::columns(const v8::FunctionCallbackInfo<v8::Value>& args){
		Isolate* isolate = args.GetIsolate();
		MatrixWrapper* obj = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		args.GetReturnValue().Set(Number::New(isolate, obj->matrix->Columns()));
	}

	void MatrixWrapper::New(const FunctionCallbackInfo<Value>& args) {
		Isolate* isolate = args.GetIsolate();

		if (args.Length() < 2) {
			// Throw an Error that is passed back to JavaScript
			isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, INSUFFICIENT_ARGS_RROR)));
			return;
		}
		// Check the argument types
		if (!args[0]->IsNumber() || !args[1]->IsNumber()) {
			isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, WRONG_ARGUMENTS_ERROR)));
			return; 
		}

		int rows = args[0]->NumberValue();
		int cols = args[1]->NumberValue();

		if(rows < 1 || rows > MAX_DIMENSION || cols < 1 || cols > MAX_DIMENSION) {
			isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, DIMS_OUTOFRANGE_ERROR)));
			return;
		}


		if (args.IsConstructCall()) {

		    MatrixWrapper* obj = new MatrixWrapper(rows, cols);
			obj->Wrap(args.This());
			args.GetReturnValue().Set(args.This());
		} else {
			// Invoked as plain function `MatrixWrapper(...)`, turn into construct call.
		    const int argc = 2;
		    Local<Value> argv[argc] = { args[0], args[1] };
		    Local<Context> context = isolate->GetCurrentContext();
		    Local<Function> cons = Local<Function>::New(isolate, constructor);
		    Local<Object> result = cons->NewInstance(context, argc, argv).ToLocalChecked();
		    args.GetReturnValue().Set(result);
		}
	}

	void MatrixWrapper::at(const FunctionCallbackInfo<Value>& args) {
		Isolate* isolate = args.GetIsolate();
		// Check the number of arguments passed.
		if (args.Length() < 2) {
		  isolate->ThrowException(Exception::TypeError( String::NewFromUtf8(isolate, INSUFFICIENT_ARGS_RROR)));
		  return;
		}

		if (!args[0]->IsNumber() || !args[1]->IsNumber()) {
			isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, WRONG_ARGUMENTS_ERROR)));
			return;
		}

		int rows = args[0]->NumberValue();
		int cols = args[1]->NumberValue();
		MatrixWrapper* obj = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix & m = * obj->matrix;

		if(rows < 0 || rows > (int)m.Rows()-1 || cols < 0 || cols > (int)m.Columns()-1) {
			isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, DIMS_OUTOFRANGE_ERROR)));
			return;
		}

			if (args.Length() > 2) {
				if(args[1]->IsNumber()) {
					m(rows, cols) = args[2]->NumberValue();
			}
			else {
				isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, NONNUMBER_ARG_RROR)));
				return;
			}
		}

		double value = m(rows, cols);
		args.GetReturnValue().Set(Number::New(isolate, value));
	}


	void MatrixWrapper::rowSum(const v8::FunctionCallbackInfo<v8::Value>& args){
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->RowSum(*arg);
	}

	void MatrixWrapper::rowSkewness(const v8::FunctionCallbackInfo<v8::Value>& args){
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->RowSkewness(*arg);
	}

	void MatrixWrapper::rowKurtosis(const v8::FunctionCallbackInfo<v8::Value>& args){
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->RowKurtosis(*arg);
	}

	void MatrixWrapper::rowVar(const v8::FunctionCallbackInfo<v8::Value>& args){
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->RowVar(*arg);
	}

	void MatrixWrapper::rowAvg(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->RowAvg(*arg);	
	}
	void MatrixWrapper::columnSum(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->ColumnSum(*arg);		
	}

	void MatrixWrapper::columnSkewness(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->ColumSkewness(*arg);	
	}

	void MatrixWrapper::columnKurtosis(const v8::FunctionCallbackInfo<v8::Value>& args){
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->ColumnKurtosis(*arg);	 
	}

	void MatrixWrapper::columnVar(const v8::FunctionCallbackInfo<v8::Value>& args){
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->ColumnVar(*arg);	 
	} 

	void MatrixWrapper::columnAvg(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->ColumnAvg(*arg);	
	}

	void MatrixWrapper::transpose(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* argument = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		matrix::Matrix * arg = argument->matrix;
		self->matrix->Transpose(*arg);
	}

	void MatrixWrapper::add(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* A = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		MatrixWrapper* B = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* out = node::ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());

		A->matrix->Add(* B->matrix, * out->matrix);
	} 

	void MatrixWrapper::sub(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* A = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		MatrixWrapper* B = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* out = node::ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());

		A->matrix->Sub(* B->matrix, * out->matrix);
	}

	void MatrixWrapper::product(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* A = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		MatrixWrapper* B = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());		
		MatrixWrapper* out = node::ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
		A->matrix->Product(* B->matrix, * out->matrix);
	}

	void MatrixWrapper::scalarProduct(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper & A = * ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		double scalar = args[0]->NumberValue();		
		MatrixWrapper & out = * node::ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
		A.matrix->Product(scalar, *out.matrix);
	}

	void MatrixWrapper::clone(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* A = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		MatrixWrapper* B = node::ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());				
		utilities::Copy(*A->matrix, *B->matrix);
	}

	void MatrixWrapper::print(const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* self = ObjectWrap::Unwrap<MatrixWrapper>(args.Holder());
		self->matrix->Print(std::cout);
	}

	MatrixWrapper::~MatrixWrapper() {
		delete this->matrix;
	}
}
