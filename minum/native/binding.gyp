{
  "targets": [
    {
	  "target_name": "minumimpl",
	  "sources": [
	    "minum-impl.cpp",
	    "matrix-wrapper.cpp",
			"linalg-wrapper.cpp"
	  ],
	  "include_dirs": [
	    "../../matrix",
			"../../mkl-linalg",
			"/opt/intel/mkl/include/"
	  ],
	  "link_settings": {
		  "ldflags": [
			    "-L/home/rsolano/workspaces/statslab-gpu/matrix/Release/",
					"-L/home/rsolano/workspaces/statslab-gpu/mkl-linalg/Release/",
					"-L/opt/intel/lib/intel64",
					"-L/opt/intel/mkl/lib/intel64/",
					"-fopenmp"
			],
	    "libraries": [
	      	'-lmatrix' ,
					'-lmkl-linalg',
					'-lmkl_intel_thread',
					'-lmkl_rt',
					'-lmkl_core',
					'-lmkl_intel_lp64',
					'-lpthread',					
					'-liomp5'
	      ]
      },
      "cflags": [
        "-std=c++11",
				"-fopenmp",
				"-fPIC"
      ]
    }
  ]
}
