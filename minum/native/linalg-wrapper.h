#ifndef LINALG_WRAPPER_H_
#define LINALG_WRAPPER_H_
#include <node.h>
#include <factorization.h>
#include <linear-eq.h>
#include <utilities.h>

namespace linalg {
    void LUFactorization(const v8::FunctionCallbackInfo<v8::Value>& args);
    void LUSolve(const v8::FunctionCallbackInfo<v8::Value>& args);

    void UCholeskyFactorization(const v8::FunctionCallbackInfo<v8::Value>& args);
    void UCholeskySolve(const v8::FunctionCallbackInfo<v8::Value>& args);

    void LCholeskyFactorization (const v8::FunctionCallbackInfo<v8::Value>& args);
    void LCholeskySolve (const v8::FunctionCallbackInfo<v8::Value>& args);

    void QRFactorization (const v8::FunctionCallbackInfo<v8::Value>& args);
    void LQFactorization (const v8::FunctionCallbackInfo<v8::Value>& args);
    void RQFactorization (const v8::FunctionCallbackInfo<v8::Value>& args);
    
    void Lower (const v8::FunctionCallbackInfo<v8::Value>& args);
    void Upper (const v8::FunctionCallbackInfo<v8::Value>& args);

    void Init(v8::Local<v8::Object> exports);
}

#endif