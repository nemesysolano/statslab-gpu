#include "linalg-wrapper.h"
#include "matrix-wrapper.h"
using namespace numjs;
using namespace v8;
using namespace node;

namespace linalg {
    void LUFactorization(const v8::FunctionCallbackInfo<v8::Value>& args) { //int LUFactorization (matrix::Matrix & in, matrix::Matrix &out);
        Isolate* isolate = args.GetIsolate(); 
		MatrixWrapper* in = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* out = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
        int result = factorization::LUFactorization(in->Matrix(), out->Matrix());
        Local<Number> number = Number::New(isolate, result);

        args.GetReturnValue().Set(number); 
    }

    void LUSolve(const v8::FunctionCallbackInfo<v8::Value>& args){ //int SolveSquare(/* inout (LU factored) */ matrix::Matrix & A, /* inout (right-hand matrix) */ matrix::Matrix & B);
        Isolate* isolate = args.GetIsolate(); 
		MatrixWrapper* LU = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* B = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
        int result = solver::LUSolve (LU->Matrix(), B->Matrix());
        Local<Number> number = Number::New(isolate, result);

        args.GetReturnValue().Set(number); 
    }

    void UCholeskyFactorization(const v8::FunctionCallbackInfo<v8::Value>& args) {
        Isolate* isolate = args.GetIsolate(); 
		MatrixWrapper* in = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* out = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
        int result = factorization::UCholeskyFactorization(in->Matrix(), out->Matrix());
        Local<Number> number = Number::New(isolate, result);

        args.GetReturnValue().Set(number);
    }

    void UCholeskySolve(const v8::FunctionCallbackInfo<v8::Value>& args) {

        Isolate* isolate = args.GetIsolate(); 
		MatrixWrapper* in = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* out = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
        int result = solver::UCholeskySolve(in->Matrix(), out->Matrix());
        Local<Number> number = Number::New(isolate, result);
 
        args.GetReturnValue().Set(number);
    }

    void LCholeskyFactorization (const v8::FunctionCallbackInfo<v8::Value>& args){
        Isolate* isolate = args.GetIsolate(); 
		MatrixWrapper* in = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* out = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
        int result = factorization::LCholeskyFactorization(in->Matrix(), out->Matrix());
        Local<Number> number = Number::New(isolate, result);

        args.GetReturnValue().Set(number);   
    }
 
    void LCholeskySolve(const v8::FunctionCallbackInfo<v8::Value>& args) {
        Isolate* isolate = args.GetIsolate(); 
		MatrixWrapper* in = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* out = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject()); 
        int result = solver::LCholeskySolve(in->Matrix(), out->Matrix());
        Local<Number> number = Number::New(isolate, result);

        args.GetReturnValue().Set(number); 
    }

    void QRFactorization (const v8::FunctionCallbackInfo<v8::Value>& args) { 
        Isolate* isolate = args.GetIsolate(); 
		MatrixWrapper* A = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* Q = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
        
        int result = factorization::QRFactorization(A->Matrix(), Q->Matrix()); 
        Local<Number> number = Number::New(isolate, result);
        args.GetReturnValue().Set(number); 
    }
 
    void LQFactorization (const v8::FunctionCallbackInfo<v8::Value>& args) {
        Isolate* isolate = args.GetIsolate(); 
		MatrixWrapper* A = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* Q = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
        
        int result = factorization::LQFactorization(A->Matrix(), Q->Matrix()); 
        Local<Number> number = Number::New(isolate, result);
        args.GetReturnValue().Set(number); 
    }

    void RQFactorization (const v8::FunctionCallbackInfo<v8::Value>& args) {
        Isolate* isolate = args.GetIsolate(); 
		MatrixWrapper* A = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* Q = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject()); 
        
        int result = factorization::RQFactorization(A->Matrix(), Q->Matrix()); 
        Local<Number> number = Number::New(isolate, result);
        args.GetReturnValue().Set(number);  
    }

    void QLFactorization (const v8::FunctionCallbackInfo<v8::Value>& args) {
        Isolate* isolate = args.GetIsolate(); 
		MatrixWrapper* A = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* Q = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
        
        int result = factorization::QLFactorization(A->Matrix(), Q->Matrix()); 
        Local<Number> number = Number::New(isolate, result);
        args.GetReturnValue().Set(number); 
    }
    
    void Lower (const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* in = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject()); 
		MatrixWrapper* out = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
        utilities::Lower(in->Matrix(), out->Matrix()); 
    }

    void Upper (const v8::FunctionCallbackInfo<v8::Value>& args) {
		MatrixWrapper* in = ObjectWrap::Unwrap<MatrixWrapper>(args[0]->ToObject());
		MatrixWrapper* out = ObjectWrap::Unwrap<MatrixWrapper>(args[1]->ToObject());
        utilities::Upper(in->Matrix(), out->Matrix()); 
    }

    void Init(Local<Object> exports) { 
        NODE_SET_METHOD(exports, "LUFactorization", LUFactorization);
        NODE_SET_METHOD(exports, "LUSolve", LUSolve);

        NODE_SET_METHOD(exports, "UCholeskyFactorization", UCholeskyFactorization);
        NODE_SET_METHOD(exports, "UCholeskySolve", UCholeskySolve);

        NODE_SET_METHOD(exports, "LCholeskyFactorization", LCholeskyFactorization);
        NODE_SET_METHOD(exports, "LCholeskySolve", LCholeskySolve);

        NODE_SET_METHOD(exports, "QRFactorization", QRFactorization);
        NODE_SET_METHOD(exports, "LQFactorization", LQFactorization);
        NODE_SET_METHOD(exports, "RQFactorization", RQFactorization);
        NODE_SET_METHOD(exports, "QLFactorization", QLFactorization);

        NODE_SET_METHOD(exports, "Lower", Lower);
        NODE_SET_METHOD(exports, "Upper", Upper); 
    }       
}