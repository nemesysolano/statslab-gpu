/*
 * matrix-wrapper.h
 *
 *  Created on: Jul 19, 2018
 *      Author: rsolano
 */

#ifndef MATRIX_WRAPPER_H_
#define MATRIX_WRAPPER_H_

#include <node.h> 
#include <node_object_wrap.h>
#include "matrix.h"
#define MAX_DIMENSION 4096
namespace numjs {

	class MatrixWrapper : public node::ObjectWrap {

		public:
			static void Init(v8::Local<v8::Object> exports);
			inline matrix::Matrix & Matrix() { return *this->matrix; }
		 private:
		  	matrix::Matrix * matrix;
			explicit MatrixWrapper(int row, int columns);
			explicit MatrixWrapper(matrix::Matrix * matrix);
			~MatrixWrapper();

			static void New(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void at(const v8::FunctionCallbackInfo<v8::Value>& args);			
			static void rowSum(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void rowSkewness(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void rowKurtosis(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void rowVar(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void rowAvg(const v8::FunctionCallbackInfo<v8::Value>& args);

			static void columnSum(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void columnSkewness(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void columnKurtosis(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void columnVar(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void columnAvg(const v8::FunctionCallbackInfo<v8::Value>& args);

			static void rows(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void columns(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void transpose(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void add(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void sub(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void product(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void scalarProduct(const v8::FunctionCallbackInfo<v8::Value>& args);

			static void clone(const v8::FunctionCallbackInfo<v8::Value>& args);
			static void print(const v8::FunctionCallbackInfo<v8::Value>& args);
			static v8::Persistent<v8::Function> constructor;

	};
}  // namespace numjs


#endif /* MATRIX_WRAPPER_H_ */
