// addon.cc
#include "matrix-wrapper.h"
#include "linalg-wrapper.h"
namespace numjs {

	using v8::Local;
	using v8::Object;

	void InitAll(Local<Object> exports) {
		MatrixWrapper::Init(exports);
		linalg::Init(exports);
	}

	NODE_MODULE(NODE_GYP_MODULE_NAME, InitAll)

}  // namespace demo
