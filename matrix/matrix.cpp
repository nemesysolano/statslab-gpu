
#include <omp.h>
#include "matrix.h"
#include <math.h>
#include <iomanip>

using namespace matrix;
using namespace std;

//TODO: Fix error messages macros and add Row/Column Avg

typedef enum MATRIX_ERROR_CODE_ENUM {
	ROWSUM_DIM_MISMATCH,
	COLUMNSUM_DIM_MISMATCH,
	TRANSPOSE_MISMATCH,
	ADD_DIM_MISMATCH,
	SUB_DIM_MISMATCH,
	PRODUCT_MISMATCH,
	ROWSKEWNESS_DIM_MISMATCH,
	COLUMNSKEWNESS_DIM_MISMATCH,
	ROWVAR_DIM_MISMATCH,
	COLUMNVAR_DIM_MISMATCH,
	ROWKURTOSIS_DIM_MISMATCH,
	COLUMNKURTOSIS_DIM_MISMATCH,
	SCALAR_PRODUCT_MISMATCH,
	COLUMNAVG_DIM_MISMATCH,
	ROWAVG_DIM_MISMATCH,
	VIEW_DIMENSIONS_OVERFLOW
} MATRIX_ERROR_CODE;

const char * MATRIX_ERROR_MESSAGES[] = {
	"ROWSUM_DIM_MISMATCH",
	"COLUMNSUM_DIM_MISMATCH",
	"TRANSPOSE_MISMATCH",
	"ADD_DIM_MISMATCH",
	"SUB_DIM_MISMATCH",
	"PRODUCT_MISMATCH",
	"ROWSKEWNESS_DIM_MISMATCH",
	"COLUMNSKEWNESS_DIM_MISMATCH",
	"ROWVAR_DIM_MISMATCH",
	"COLUMNVAR_DIM_MISMATCH",
	"ROWKURTOSIS_DIM_MISMATCH",
	"COLUMNKURTOSIS_DIM_MISMATCH",
	"SCALAR_PRODUCT_MISMATCH",
	"COLUMNAVG_DIM_MISMATCH",
	"ROWAVG_DIM_MISMATCH",
	"VIEW_DIMENSIONS_OVERFLOW"
};
/** Store the same value in all cells.
 * @param v
 */
void Matrix::All(double v) {
	const unsigned length = rows * columns;
	double * buffer = this->buffer;
	size_t i;

#pragma omp parallel for
    for(i = 0; i < length; i++)
    {
    	buffer[i] = v;
    }
}


/** Creates a rows_ * columns_ matrix.
 * To enable openmp in client programs as described in https://devtalk.nvidia.com/default/topic/387493/how-use-openmp-in-cu-file-/
 */
Matrix::Matrix(
	double init, /**< Initial value*/
	unsigned rows_,  /**< Vertical dimension*/
	unsigned columns_ /**< Horizontal dimension*/
):
	rows(rows_), columns(columns_)
{
	const unsigned length = rows * columns;
	buffer = (double *)malloc(rows * columns * sizeof(double));
	size_t i;

#pragma omp parallel for
    for(i = 0; i < length; i++)
    {
    	buffer[i] = init;
    }
}

/** Copy constructor */
Matrix::Matrix (const Matrix& other /**< The source matrix */) :
	buffer(other.buffer), rows(other.rows), columns(other.columns), owner(false)
{

}


/** Copy assignment operator */
Matrix& Matrix::operator = (const Matrix& other /**< The source matrix */)
{
	rows = other.rows;
	columns = other.columns;
	owner = false;
	buffer = other.buffer;

	return *this;
}

/** Move constructor */
Matrix::Matrix (Matrix&& other /**< The source matrix */) noexcept:
		buffer(other.buffer), rows(other.rows), columns(other.columns), owner(true) {
	other.Clear();
}


/** Move assignment operator */
Matrix& Matrix::operator = (Matrix&& other /**< The source matrix */) noexcept
{
    this->rows = other.rows;
    this->columns = other.columns;
    this->owner = true;
    this->buffer = other.buffer;
    other.Clear();
    return *this;
}

void Matrix::Clear() {
	this->buffer = nullptr;
	this->rows = 0;
	this->columns = 0;
}

void Matrix::Destroy() {
	if(buffer != nullptr && owner) {
		free(buffer);
		Clear();
	}
}

/**
 * @param out A Column vector whose length is equals to this->rows
 */
void Matrix::RowSum(Matrix & out) {
	if(!(out.rows == this->rows && out.columns == 1))
		THROW_ERROR(ROWSUM_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t i;

#pragma omp parallel for
	for(i = 0; i < self.rows; i++) {
		double sum = 0;
		for(size_t k = 0; k < self.columns; k++) {
			sum += self(i,k);
		}
		out(i, 0) = sum;
	}

}

/**
 * @param out A Row vector whose length is equals to this->columns
 */
void Matrix::ColumnSum(Matrix & out) {
	if(!(out.rows == 1 && out.columns == this->columns))
		THROW_ERROR(COLUMNSUM_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t k;

#pragma omp parallel for
	for(k = 0; k < self.columns; k++) {
		double sum = 0;
		for(size_t i = 0; i < self.rows; i++) {
			sum += self(i,k);
		}
		out(0, k) = sum;
	}


}

void Matrix::Transpose(Matrix & out){
	if(!(out.rows == columns && out.columns == rows))
		THROW_ERROR(TRANSPOSE_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t k;

#pragma omp parallel for
	for(k = 0; k < columns; k++) {
		for(size_t i = 0; i < rows; i++) {
			out(k,i) = self(i,k);
		}
	}
}

void Matrix::Add(Matrix & B, Matrix & out) {
	if(!(
			B.rows == rows && B.columns == columns &&
			out.rows == rows && out.columns == columns
		)
	)
		THROW_ERROR(ADD_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t k;

#pragma omp parallel for
	for(k = 0; k < columns; k++) {
		for(size_t i = 0; i < rows; i++) {
			out(i,k) = self(i,k) + B(i,k);
		}
	}

}

void Matrix::Sub(Matrix & B, Matrix & out) {
	if(!(
			B.rows == rows && B.columns == columns &&
			out.rows == rows && out.columns == columns
		)
	)
		THROW_ERROR(SUB_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t k;

#pragma omp parallel for
	for(k = 0; k < columns; k++) {
		for(size_t i = 0; i < rows; i++) {
			out(i,k) = self(i,k) - B(i,k);
		}
	}
}

#define TRUNC(a,epsilon) (a < epsilon ? 0 : a);
void Matrix::Product(Matrix & B, Matrix & out){

	Matrix & A = *this;
	Matrix & C = out;

	if(A.columns != B.rows)
		THROW_ERROR(PRODUCT_MISMATCH, MATRIX_ERROR_MESSAGES);
	size_t r1 = A.rows;
	size_t r2 = B.rows;
	size_t c2 = B.columns;
	size_t i;

#pragma omp parallel for
    for (i = 0; i < r1; i++)
    {
        for (size_t j = 0; j < c2; j++)
        {
            C(i,j) = 0;
            for (size_t k = 0; k < r2; k++)
            {
                C(i,j) += A(i,k) * B(k,j);
            }
            //C(i,j) = TRUNC(C(i,j), EPSILON);
        }
    }
}

void Matrix::Product(double scalar, Matrix & out) {
	size_t k;
	Matrix & self = *this;
	if(!(out.rows == rows && out.columns == columns))
		THROW_ERROR(SCALAR_PRODUCT_MISMATCH, MATRIX_ERROR_MESSAGES);

#pragma omp parallel for
	for(k = 0; k < columns; k++) {
		for(size_t i = 0; i < rows; i++) {
			out(i,k) = self(i,k) * scalar;
		}
	}
}

void Matrix::Eye(Matrix & out) {
	size_t k;

#pragma omp parallel for
	for(k = 0; k < out.columns; k++) {
		for(size_t i = 0; i < out.rows; i++) {
			out(i,k) = i == k ? 1.0 : 0.0;
		}
	}

}

void Matrix::RowSkewness(Matrix &out) {
	if(!(out.rows == this->rows && out.columns == 1))
		THROW_ERROR(ROWSKEWNESS_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t n = self.columns;
	size_t i;

//Calculates means
#pragma omp parallel for
	for(i = 0; i < self.rows; i++) {
		double sum = 0;
		for(size_t k = 0; k < self.columns; k++) {
			sum += self(i,k);
		}
		out(i, 0) = sum / n;
	}

//Calculates errors
#pragma omp parallel for
	for(i = 0; i < self.rows; i++) {
		double diff = 0;
		double diff2 = 0;
		double diff3 = 0;
		double sum_diff2 = 0;
		double sum_diff3 = 0;

		for(size_t k = 0; k < self.columns; k++) {
			diff = (self(i,k) - out(i, 0));
			diff2 = diff * diff;
			diff3 = diff2 * diff;

			sum_diff2 += diff2;
			sum_diff3 += diff3;
		}
		out(i, 0) = ((1.0/n) * sum_diff3) / pow(sqrt(((1.0/(n-1)) * sum_diff2)), 3.0);
	}
}

void Matrix::RowKurtosis(Matrix &out) {
	if(!(out.rows == this->rows && out.columns == 1))
		THROW_ERROR(ROWKURTOSIS_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t n = self.columns;
	size_t i;

//Calculates means
#pragma omp parallel for
	for(i = 0; i < self.rows; i++) {
		double sum = 0;
		for(size_t k = 0; k < self.columns; k++) {
			sum += self(i,k);
		}
		out(i, 0) = sum / n;
	}

//Calculates errors
#pragma omp parallel for
	for(i = 0; i < self.rows; i++) {
		double diff = 0;
		double diff2 = 0;
		double diff4 = 0;
		double sum_diff2 = 0;
		double sum_diff4 = 0;

		for(size_t k = 0; k < self.columns; k++) {
			diff = (self(i,k) - out(i, 0));
			diff2 = diff * diff;
			diff4 = diff2 * diff2;

			sum_diff2 += diff2;
			sum_diff4 += diff4;
		}

		double variance = ((1.0/(n-1)) * sum_diff2);
		out(i, 0) = ((1.0/n) * sum_diff4) / (variance * variance);
	}
}

void Matrix::RowVar(Matrix &out) {
	if(!(out.rows == this->rows && out.columns == 1))
		THROW_ERROR(ROWVAR_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t n = self.columns;
	size_t i;
//Calculates means
#pragma omp parallel for
	for(i = 0; i < self.rows; i++) {
		double sum = 0;
		for(size_t k = 0; k < self.columns; k++) {
			sum += self(i,k);
		}
		out(i, 0) = sum / n;
	}

//Calculates errors
#pragma omp parallel for
	for(i = 0; i < self.rows; i++) {
		double diff = 0;
		double diff2 = 0;
		double sum_diff2 = 0;

		for(size_t k = 0; k < self.columns; k++) {
			diff = (self(i,k) - out(i, 0));
			diff2 = diff * diff;
			sum_diff2 += diff2;
		}

		double variance = ((1.0/(n-1)) * sum_diff2);
		out(i, 0) = variance;
	}
}

void Matrix::ColumSkewness(Matrix &out) {
	if(!(out.rows == 1 && out.columns == this->columns))
		THROW_ERROR(COLUMNSKEWNESS_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t n = self.rows;
	size_t k;

//Calculates means
#pragma omp parallel for
	for(k = 0; k < self.columns; k++) {
		double sum = 0;
		for(size_t i = 0; i < self.rows; i++) {
			sum += self(i,k);
		}
		out(0, k) = sum / n;
	}

//Calculates errors
#pragma omp parallel for
	for(k = 0; k < self.columns; k++) {
		double diff = 0;
		double diff2 = 0;
		double diff3 = 0;
		double sum_diff2 = 0;
		double sum_diff3 = 0;

		for(size_t i = 0; i < self.rows; i++) {
			diff = (self(i,k) - out(0, k));
			diff2 = diff * diff;
			diff3 = diff2 * diff;

			sum_diff2 += diff2;
			sum_diff3 += diff3;
		}
		out(0, k) = ((1.0/n) * sum_diff3) / pow(sqrt(((1.0/(n-1)) * sum_diff2)), 3.0);
	}
}

void Matrix::ColumnKurtosis(Matrix &out) {
	if(!(out.rows == 1 && out.columns == this->columns))
		THROW_ERROR(COLUMNKURTOSIS_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t n = self.rows;
	size_t k;

//Calculates means
#pragma omp parallel for
	for(k = 0; k < self.columns; k++) {
		double sum = 0;
		for(size_t i = 0; i < self.rows; i++) {
			sum += self(i,k);
		}
		out(0, k) = sum / n;
	}

//Calculates errors
#pragma omp parallel for
	for(k = 0; k < self.columns; k++) {
		double diff = 0;
		double diff2 = 0;
		double diff4 = 0;
		double sum_diff2 = 0;
		double sum_diff4 = 0;

		for(size_t i = 0; i < self.rows; i++) {
			diff = (self(i,k) - out(0, k));
			diff2 = diff * diff;
			diff4 = diff2 * diff2;

			sum_diff2 += diff2;
			sum_diff4 += diff4;
		}

		double variance = ((1.0/(n-1)) * sum_diff2);
		out(0, k) = ((1.0/n) * sum_diff4) / (variance * variance);
	}
}

void Matrix::ColumnVar(Matrix & out){
	if(!(out.rows == 1 && out.columns == this->columns))
		THROW_ERROR(COLUMNVAR_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t n = self.rows;
	size_t k ;

//Calculates means
#pragma omp parallel for
	for(k = 0; k < self.columns; k++) {
		double sum = 0;
		for(size_t i = 0; i < self.rows; i++) {
			sum += self(i,k);
		}
		out(0, k) = sum / n;
	}

//Calculates errors
#pragma omp parallel for
	for(k = 0; k < self.columns; k++) {
		double diff = 0;
		double diff2 = 0;
		double sum_diff2 = 0;

		for(size_t i = 0; i < self.rows; i++) {
			diff = (self(i,k) - out(0, k));
			diff2 = diff * diff;

			sum_diff2 += diff2;
		}

		double variance = ((1.0/(n-1)) * sum_diff2);
		out(0, k) = variance;
	}
}



void Matrix::RowAvg(Matrix &out) {
	if(!(out.rows == this->rows && out.columns == 1))
		THROW_ERROR(ROWAVG_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t n = self.columns;
	size_t i;
//Calculates means
#pragma omp parallel for
	for(i = 0; i < self.rows; i++) {
		double sum = 0;
		for(size_t k = 0; k < self.columns; k++) {
			sum += self(i,k);
		}
		out(i, 0) = sum / n;
	}
}


void Matrix::ColumnAvg(Matrix & out){
	if(!(out.rows == 1 && out.columns == this->columns))
		THROW_ERROR(COLUMNAVG_DIM_MISMATCH, MATRIX_ERROR_MESSAGES);

	Matrix & self = *this;
	size_t n = self.rows;
	size_t k ;

//Calculates means
#pragma omp parallel for
	for(k = 0; k < self.columns; k++) {
		double sum = 0;
		for(size_t i = 0; i < self.rows; i++) {
			sum += self(i,k);
		}
		out(0, k) = sum / n;
	}
}

Matrix::~Matrix() {Destroy();}

void Matrix::Print(ostream &out) {
	Matrix & m = *this;
	size_t k;
	for(size_t i = 0; i < m.Rows(); i++) {
		for(k = 0; k < m.Columns()-1; k++) {
			out <<  std::fixed << std::setprecision(5) << std::setw(12)  <<  m(i,k);
			out << ',';
		}
		out <<  std::fixed << std::setprecision(5) << std::setw(12) << m(i, k) << endl;
	}
}


//TODO: Implement orthogonal solvers using https://utd.edu/~ammann/stat6341/node9.html
View::View(Matrix & source, size_t row, size_t column, size_t rows, size_t columns) {
	if(
		row + rows > source.Rows() ||
		column + columns > source.Columns()
	) {
		THROW_ERROR(VIEW_DIMENSIONS_OVERFLOW, MATRIX_ERROR_MESSAGES);
	}
	this->owner = false;
	this->rows = rows;
	this->columns = columns;
	this->buffer = (buffer + row * source.Columns() + column);
}
