/*
 * matrix.h
 *
 *  Created on: Mar 8, 2017
 *      Author: rsolano
 */

#ifndef MATRIX_H_
#define MATRIX_H_
#include <cstdlib>
#include <iostream>

/*
 Contribution:
 	 http://docs.cython.org/en/latest/src/userguide/memoryviews.html#pass-data-from-a-c-function-via-pointer
 	 https://github.com/cython/cython/wiki/tutorials-NumpyPointerToC
*/
namespace matrix {
	/** This exception reports issues that may occur inside Matrix class' methods.
	 *
	 */
	class MatrixError: public std::runtime_error {
	protected:
		int code = 0 /**< Custom error message. */;
	public:
		/** calls runtime_error(const string &) and initializes this->code;
		 *
		 */
		 inline explicit MatrixError (int code_ /**< Custom error code. */, const std::string& what_arg /**< Custom error message. */): runtime_error(what_arg), code(code_) {

		 }

		 /** calls runtime_error(const char *) and initializes this->code;
		  *
		  */
		 inline explicit MatrixError (int code_ /**< Custom error code. */, const char* what_arg /**< Custom error message. */): runtime_error(what_arg), code(code_) {

		 }

		 /**
		  * @return this->code
		  */
		 inline int Code() { return code; }
	};

	/** This class implements a M * N Matrix.
	 *  It follows the rule of five as required by C++11
	 */
	class Matrix {
		protected:
			double * buffer = nullptr;
			unsigned rows = 0;
			unsigned columns = 0;
			bool owner = true;

		private:
			/** Releases allocated buffer.
			 *
			 */


			/** Sets buffer = nullptr and rows, columns = 0.
			 *
			 */
			void Clear();

		public:
			/** Releases allocated buffer and sets rows and columns to zero and buffer to nullptr.
			 *	Creates an empty matrix.
			 */
			inline Matrix() {}

			inline Matrix (double * buffer_, unsigned rows_, unsigned columns_):buffer(buffer_), rows(rows_), columns(columns_), owner(false) {};

		    /** Copy constructor */
			Matrix (const Matrix& other /**< The source matrix */) ;

		    /** Move constructor */
			Matrix (Matrix&& other /**< The source matrix */) noexcept;

			/** Copy operator */
			Matrix& operator = (const Matrix& other /**< The source matrix */);

			/** Move assignment operator */
			Matrix& operator = (Matrix&& other /**< The source matrix */) noexcept;


			inline double * Buffer() { return buffer;}; // Be careful with direct buffer manipulation!!



			/** Creates a rows_ * columns_ matrix.
			 *
			 */
			inline Matrix(
				unsigned rows_, /**< Vertical dimension*/
				unsigned columns_  /**< Horizontal dimension*/
			):
					rows(rows_), columns(columns_){ buffer = (double *)calloc(rows * columns, sizeof(double));}

			/** Creates a rows_ * columns_ matrix.
			 *
			 */
			Matrix(
				double init, /**< Initial value*/
				unsigned rows_,  /**< Vertical dimension*/
				unsigned columns_ /**< Horizontal dimension*/
			);

			/** Returns true if this matrix is empty.
			 *
			 */
			inline bool IsEmpty() {return buffer == nullptr;}

			/** Returns a reference to the value @(row, column) position.
			 *  @return a double.
			 */
			inline double & operator () (unsigned row, unsigned column) {return buffer[row * columns + column];} //TODO: Add boundary check.

			/** Reports the vertical dimension.
			 * @return Row count.
			 */
			inline unsigned Rows() { return rows; }

			/** Reports the horizontal dimension.
			 * @returns count.
			 */
			inline unsigned Columns() {return columns;}

			/**
			 * @param out A Colum vector whose length is equals to this->rows
			 */
			void RowSum(Matrix & out);
			inline Matrix RowSum() {Matrix out(this->rows,1); RowSum(out); return out;};

			void RowSkewness(Matrix &out);
			inline Matrix RowSkewness() {Matrix out(1, this->columns); RowSkewness(out); return out;}

			void RowKurtosis(Matrix &out);
			inline Matrix RowKurtosis() {Matrix out(1, this->columns); RowKurtosis(out); return out;}

			void RowVar(Matrix &out);
			inline Matrix RowVar() {Matrix out(1, this->columns); RowVar(out); return out;}

			void RowAvg(Matrix &out);
			inline Matrix RowAvg() {Matrix out(1, this->columns); RowAvg(out); return out;}

			/**
			 * @param out A Row vector whose length is equals to this->columns
			 */
			void ColumnSum(Matrix & out);
			inline Matrix ColumnSum() {Matrix out(1, this->columns); ColumnSum(out); return out;}

			void ColumSkewness(Matrix &out);
			inline Matrix ColumSkewness() {Matrix out(1, this->columns); ColumSkewness(out); return out;}

			void ColumnKurtosis(Matrix &out);
			inline Matrix ColumnKurtosis() {Matrix out(1, this->columns); ColumnKurtosis(out); return out;}

			void ColumnVar(Matrix &out);
			inline Matrix ColumnVar() {Matrix out(1, this->columns); ColumnVar(out); return out;}

			void ColumnAvg(Matrix &out);
			inline Matrix ColumnAvg() {Matrix out(1, this->columns); ColumnAvg(out); return out;}

			/** Store the same value in all cells.
			 * @param v
			 */
			void All(double v);
			/** Releases allocated buffer and sets rows and columns to zero and buffer to nullptr.
			 *
			 */

			void Transpose(Matrix & out);

			inline Matrix Transpose() {Matrix out(columns, rows); Transpose(out); return out;}

			void Add(Matrix & B, Matrix & out);

			inline Matrix Add(Matrix & B) { Matrix out(rows, columns); Add(B, out); return out;}

			void Sub(Matrix & B, Matrix & out);

			inline Matrix Sub(Matrix & B) { Matrix out(rows, columns); Sub(B, out); return out;}

			void Product(Matrix & B, Matrix & out);

			inline Matrix Product(Matrix & B) { Matrix out(Rows(), B.Columns()); Product(B, out); return out;}

			void Product(double scalar, Matrix & out);

			inline Matrix Product(double scalar) { Matrix out(rows, columns); Product(scalar, out); return out;}

			static void Eye(Matrix & out);

			~Matrix();

			void Print(std::ostream &out);

			void Destroy();
	};


	class View : public Matrix {

		public:
			View(Matrix & source, size_t row, size_t column, size_t rows, size_t columns);
	};
}


#define THROW_ERROR(code, messages) throw matrix::MatrixError(code, messages[code])

#endif /* MATRIX_H_ */
