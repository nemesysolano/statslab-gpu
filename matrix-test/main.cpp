#include <matrix.h>
#include <linear-eq.h>
#include <iostream>
#include <factorization.h>
#include <iomanip>

//http://bowdoin.edu/~ltoma/teaching/cs3225-GIS/fall17/Lectures/openmp.html
//https://stackoverflow.com/questions/39506522/setting-up-nsight-with-openmp

/*
1,2,3,1
5,7,11,1
13,17,19,1


1,2,3,1
5,7,11,1
13,17,19,1

 */
using namespace matrix;
using namespace std;


int main(int argc, char * argv[]) {
	Matrix m(3,3), M(3,3), B(3,1);

	m(0,0)=  5; m(0,1)=  19; m(0,2)=  3;
	m(1,0)=  1; m(1,1)=  7; m(1,2)= 23;
	m(2,0)= 17; m(2,1)=  2; m(2,2)= 11;

	B(0,0)=14;
	B(1,0)=15;
	B(2,0)=29;

	m.Print(cout);
	cout << endl;

	if(solver::LUSolve(m,M,B) == 0) {
		M.Print(cout);
		cout << endl;
		B.Print(cout);
		cout << endl;
		m.Product(B).Print(cout);
	}



	return 0;
}


